﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Loggers
{
    /// <summary>
    /// create a file "logger_[current date].log" in the repository [application]\Debug\bin\" 
    /// The headers are "timestamp","level" and "message" with using tabulations as the seperation
    /// </summary>
    public class Logger
    {

        #region 
        private string path;
        private string date;
        private string file;
        private List<string> content;
       

        #endregion private


        #region public
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="date"></param>
        public Logger(string path, string date)

        {
            this.path = path;
            this.date = date;

        }


        #endregion public

        #region constructor 
        /// <summary>
        /// 
        /// </summary>
        public List<string> Content {


            get
            {
                this.content = new List<string>();
                using (StreamReader sr = new StreamReader(path + "/" + file))
                {
                   
                    string secondLine = File.ReadLines(path + "/" + file).ElementAtOrDefault(0);
                    while ((secondLine = sr.ReadLine()) != null) { 
                        this.content.Add(secondLine);

                    }
                   

                    sr.Close(); //close the file
                  
                }
                return this.content;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expectedLevel"></param>
        /// <param name="expectedMessage"></param>
        public void WriteEntry(string expectedLevel, string expectedMessage)
        {
            file = "logger_["+ date +"].log";



            //If the file does not exists , it creates a new file
            if (!File.Exists(Path.Combine(path,file))) {

                FileStream fs = new FileStream(path + "/" + file, FileMode.Create); //create the file
               
                //Writting headers into the file
              /*  using (StreamWriter sw = new StreamWriter(path + "/" + file))
                {

                    sw.WriteLine("Timestamp\tLevel\tMessage\r\n"); //Write Line into the files

                }*/

                fs.Close(); //close the file
            }

            using (StreamWriter sw = new StreamWriter(path + "/" + file)) {
                
                    sw.WriteLine(date +"\t"+expectedLevel+ "\t" + expectedMessage + "\r\n"); //Write Line into the files

                sw.Close(); //close the file
            }
           
            

        }


        #endregion construtor


    }
}
